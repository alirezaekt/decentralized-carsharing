export default {
  server:{
    port:8000,
    // in server add this line
    host:'0.0.0.0'
  },
  // env: {
  //   baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  // },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Carsharing',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Material+Icons+Outlined' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Material+Icons+Round' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' }
    ],
    script:[
      {src:'https://unpkg.com/axios/dist/axios.min.js'}
      
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'assets/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/vue-qrcode"},
    { src: '~/plugins/persistedState.client.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    //https://github.com/nuxt-community/dotenv-module
    ['@nuxtjs/dotenv', { filename: '.env' }]
  ],
  router:{
    middleware: ['auth']
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://nuxt-socket-io.netlify.app/installation
    'nuxt-socket-io',
    // https://github.com/chantouchsek/vuejs-google-maps
    // ['vuejs-google-maps/nuxt', {apiKey: 'AIzaSyCPgnUREvo41CXQH_YrK6f0QXewnUxTmSo', libraries: [/* rest of libraries */]}]
    
    ['nuxt-tailvue', {all: true}]
  ],
  io: {
    // Options
    sockets: [{
      name: 'verity',
      url: process.env.VUE_APP_VERITY_AGENT
    }]
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
