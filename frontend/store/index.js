export const state = () => ({
    verification_method:null,
    user: null, //redirect did
    relationshipDid:null,
    driver_licence_status:null
    
})

export const mutations = {
    
    setVerificationMethod(state, method) {
      state.verification_method = method
    },
    setUser(state, user) {
      state.user = user
    },
    setRelationDid(state, reldid) {
      state.relationshipDid = reldid
    },
    setDriverLicenceStatus(state, status) {
      state.driver_licence_status = status
    },

}