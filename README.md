## Decentralized Carsharing SSI Authentication Prototype
In this prototype users can login using various SSI options such as Connect.Me,Jolocom and ONTO.

### Installation

Requirements:
- NodeJs 12 or above
- ngrok ([https://ngrok.com/])(for exposing localhost to external APIs)
- Domain DID and REST API key from Evernym for Connect.Me Login
- Go for ONT iD backend

#### Frontend

in /frontend:
```sh
npm install
```
```sh
npm run dev
```
make a .env and add these values:
```sh
VUE_APP_VERITY_AGENT=http://localhost:3000
VUE_APP_ONTO_AGENT=http://localhost:3100
VUE_APP_VERITY_ISSUER_DID=<use did generated from /issuer page>
VUE_APP_VERITY_ISSUER_VERKEY=<use key generated from /issuer page>
```
Open the app at https://localhost:8000

#### Node Backend (for Verity / Jolocom login)
To try out the application follow these steps:
- In a separate terminal window start ngrok for port 3000 and leave it running (for Connect.Me Login):
```sh
ngrok http 3000
```
in /backend_node:
- Install required NodeJs packages:
```sh
npm install
```
- Start the web app
```sh
node app.js
```
make a .env file and add these values:
```sh 
VERITY_URL=https://vas.pps.evernym.com
DOMAIN_DID=<copy domain DID received from Evernym>
X_API_KEY=<copy API key received from Evernym>
WEBHOOK_URL=<copy public URL address of the Ngrok forwarding tunnel to your local port 3000>
```

#### Go Backend (for ONT login)
in /backend_go:
```sh
go mod download
```
```sh
go run .
```

### ToDo

- [x] Adding driverlicence VC verification (ConnectMe)
- [x] Adding ONTO authentication
- [ ] Adding Jolocom integration
- [ ] Adding Helix ID integration